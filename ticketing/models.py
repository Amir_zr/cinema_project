from django.db import models

class Movie(models.Model):

    class Meta:

        verbose_name = 'فیلم'
        verbose_name_plural = 'فیلم'


    name = models.CharField('نام فیلم' ,max_length=50)
    director = models.CharField('کارگردان' ,max_length=50)
    year = models.IntegerField('سال تولید')
    length = models.IntegerField('مدت زمان فیلم')
    desciption = models.TextField('توضیحات')
    poster = models.ImageField('پوستر', upload_to='movie_posters/' , blank=True)

    def __str__(self):
        return self.name

class Cinema(models.Model):

    class Meta:
        verbose_name = 'سینما'
        verbose_name_plural = 'سینما'
    name = models.CharField('نام' ,max_length=50)
    city = models.CharField('نام شهر' ,max_length=50 , default='تهران')
    capacity = models.IntegerField('ظرفیت')
    phone = models.CharField('تلفن' ,max_length=30 , null=True)
    address = models.TextField('آدرس')
    image = models.ImageField('تصویر', upload_to='cinema_images/' , null=True , blank=True)

    def __str__(self):
        return self.name


class ShowTime(models.Model):

    class Meta:

        verbose_name = 'سانس'
        verbose_name_plural = 'سانس'

    movie = models.ForeignKey('Movie' , on_delete=models.PROTECT ,verbose_name='فیلم')
    cinema = models.ForeignKey('Cinema' , on_delete=models.PROTECT ,verbose_name='سینما')

    start_time = models.DateTimeField('زمان شروع فیلم')
    price = models.IntegerField('قیمت')
    salable_seats = models.IntegerField('صندلی های قابل فروش')
    free_seats = models.IntegerField('صندلی های خالی')

    SALE_NOT_STARTED = 1
    SALE_OPENED = 2
    TICKETED_SOLD = 3
    SALE_CLOSED = 4
    MOVIE_PLAY = 5
    SHOW_CANCELED = 6
    status_choices = (
        (SALE_NOT_STARTED, 'فروش آغز نشده'),
        (SALE_OPENED, 'درحال فروش بلیت'),
        (TICKETED_SOLD, 'بلیت ها تمام شد'),
        (SALE_CLOSED, 'فروش بلیت تمام شد'),
        (MOVIE_PLAY, 'فیلم پخش شد'),
        (SHOW_CANCELED, 'سانس لغو شد')
    )
    status = models.IntegerField('وضعیت' ,choices=status_choices)

    def __str__(self):
        return '{} - {} - {}'.format(self.movie, self.cinema, self.start_time)


    def get_price_display(self):
        return '{}تومان'.format(self.price)

    def is_full(self):
        # return true if all seats sre sold #
        return self.free_seats == 0

    def open_sale(self):
        """
        opens ticket sale
        if sale was opened before, raise an exception
        """
        if self.status == ShowTime.SALE_NOT_STARTED:
            self.status = ShowTime.SALE_OPENED
            self.save()
        else:
            raise Exception('sale has been started before')

    def close_sale(self):
        """
        closes ticket sale
        if sale is mot open, raises an exception
        """
        if self.status == ShowTime.SALE_OPENED:
            self.status = ShowTime.SALE_CLOSED
            self.save()
        else:
            raise Exception('Sale is not open')

    def expire_showtime(self, is_canceled=False):
        """
        Expire showtime and updates the status
        :param is_canceled: A boolean indicating whether the show is canceled or not
        """
        if self.status not in (ShowTime.MOVIE_PLAY, ShowTime.SHOW_CANCELED):
            self.status = ShowTime.SHOW_CANCELED if is_canceled else ShowTime.MOVIE_PLAY
            self.save()
        else:
            raise  Exception('show has been expired before')

    def reserve_seats(self, seat_count):
        """
        Reseves one or more seats for a customer
        :param seat_count: An integer as the number of set ti be reserved
        """
        assert isinstance(seat_count, int) and seat_count>0 , 'Number of seat showtime is not integer'
        assert self.status == ShowTime.SALE_OPENED, 'sale is not open'
        assert self.free_seats >= seat_count, 'Not enough free seats'
        self.free_seats -= seat_count
        if self.free_seats == 0:
            self.status = ShowTime.TICKETED_SOLD
        self.save()

class Ticket(models.Model):
    """
    represents one or more tickets, bought by a user in an order
    """

    class Meta:
        verbose_name = 'بلیت'
        verbose_name_plural = 'بلیت'

    showtime = models.ForeignKey('ShowTime', on_delete=models.PROTECT, verbose_name='سانس')
    customer = models.ForeignKey('accounts.Profile', on_delete=models.PROTECT, verbose_name='خریدار')
    seat_count = models.IntegerField('تعداد صندلی')
    order_time = models.DateTimeField('زمان خرید', auto_now_add=True)

    def __str__(self):
        return '{}بلیت به نام{}برای فیلم{}'.format(self.seat_count, self.customer, self.showtime.movie)

